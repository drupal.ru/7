<?php
/**
 * @file
 */

/**
 * Preprocess the primary theme implementation for a view.
 */
function druru_preprocess_views_view(&$vars) {
  $view = $vars['view'];

  $views_tracker = ['tracker', 'tracker_new', 'tracker_my', 'tracker_featured'];
  $views_user    = ['user_blog', 'user_comments'];

  if (in_array($view->name, array_merge($views_tracker, $views_user))) {
    $vars['classes_array'] = [];
    $vars['classes_array'][] = 'entity-listing';
    $vars['classes_array'][] = 'entity-listing--' . drupal_clean_css_identifier($vars['name']);
  }

  if (in_array($view->name, $views_tracker)) {
    $vars['classes_array'][] = 'entity-listing--' . $view->current_display;
  }

  if (in_array($view->name, $views_user)) {
    $vars['classes_array'][] = 'entity-listing--extended';
  }

  if ($view->name == 'advertisement_blocks') {
    $vars['classes_array'] = [
      'view',
      'view-dntn'
    ];
  }
}

function druru_views_tree_inner($vars) {
  $view = $vars['view'];
  $options = $vars['options'];
  $rows = $vars['rows'];
  $title = $vars['title'];
  $result = $vars['result'];
  $parent = $vars['parent'];

  // Use function _term_depth() from ctools
  module_load_include('inc', 'ctools', 'term_depth/plugins/access/term_depth');

  $items = array();
  foreach ($result as $i => $record) {
    if ($record->views_tree_parent == $parent) {
      $term_depth = _term_depth($record->tid);

      $vars['parent'] = $record->views_tree_main;

      $items[] = [
        'data' => $rows[$i] . call_user_func(__FUNCTION__, $vars),
        'class' => ['documentation-category'],
      ];
    }
  }

  return count($items) ? theme('div_list', ['items' => $items, 'attributes' => ['class' => 'documentation-categories--level-' . $term_depth]]) : '';
}
