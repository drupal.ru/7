<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="documentation-categories--level-1">
  <?php if (!empty($title)): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php $last_id = 0; ?>
  <div class="documentation-guides">
    <?php foreach ($rows as $id => $row): ?>
      <div class="node guide">
        <?php print $row; ?>
        <?php $last_id = $id ?>
      </div>
    <?php endforeach; ?>
  </div>
  <?php print $raw_result[$last_id]['field_proposed_guides']; ?>
</div>
