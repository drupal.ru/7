<?php

/**
 * Implements hook_form_FORM_ID_alter().
 */
function druru_form_search_api_page_search_form_alter(&$form, &$form_state) {
  unset($form['form']['keys_1']['#title']);
  $form['form']['keys_1']['#attributes']['placeholder'] = t('Search');
}
