<?php

/**
 * Implements hook_preprocess_field().
 * @todo Refactor to use css classes
 */
function druru_preprocess_field(&$vars) {
  $element = &$vars['element'];
  $label   = &$vars['label'];

  // Assign class name to body of nodes and comments
  if (in_array($element['#field_name'], ['body', 'comment_body'])) {
    $vars['classes_array'] = [];
    $vars['classes_array'][] = $element['#entity_type'] . '__body';
  }

  // Set single tpl for group of taxonomy fields
  if (in_array($element['#field_name'], ['taxonomy_vocabulary_7', 'taxonomy_vocabulary_8', 'taxonomy_vocabulary_10', 'taxonomy_forums'])) {
    $vars['theme_hook_suggestions'][] = 'field__group_taxonomy';
    $vars['theme_hook_suggestions'][] = 'field__group_taxonomy__' . $element['#entity_type'];
    $vars['theme_hook_suggestions'][] = 'field__group_taxonomy__' . $element['#entity_type'] . '__' . $element['#view_mode'];
  }

  if ($element['#field_name'] == 'field_guide_authors') {
    $authors = $vars['items'];

    // Prepare variable for 'promo signature' to be used in field template
    foreach ($authors as $key => $author) {
      $promo_signature = !empty($author['#item']['entity']->field_promo_signature) ? $author['#item']['entity']->field_promo_signature[LANGUAGE_NONE][0]['safe_value'] : NULL;
      $vars['items'][$key]['promo_signature'] = $promo_signature ? ' <span class="user__promo">' . $promo_signature . '</span>' : '';
    }

    // Change field label according to number of referenced users
    $vars['label'] = count($vars['items']) > 1 ? t('Authors') : t('Author');
  }
}
