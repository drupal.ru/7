<?php

/**
 * Process variables for user-profile.tpl.php.
 *
 * @param array $variables
 *     An associative array containing:
 *     - elements: An associative array containing the user information and any
 *     fields attached to the user. Properties used:
 *     - #account: The user account of the profile being viewed.
 *
 * @see user-profile.tpl.php
 */
function druru_preprocess_user_profile(&$variables) {
  if (empty($variables['elements']['#account'])) {
    return;
  }

  // Preprocess profile groups
  _druru_prepare_profile_groups($variables);

  _druru_prepare_order($variables['user_profile']);

  $uid = $variables['elements']['#account']->uid;
  $name = $variables['elements']['#account']->name;

  if (!dru_antispam_has_verified($uid)) {
    $variables['classes_array'][] = 'is-unverified';
  }

  if (user_is_blocked($name) != FALSE) {
    $variables['classes_array'][] = 'is-blocked';
  }

  $variables['attributes_array']['class'] = $variables['classes_array'];
}

function _druru_prepare_profile_groups(&$variables) {
  $profile = &$variables['user_profile'];
  $elements = &$variables['elements'];
  $account = $elements['#account'];
  _druru_prepare_private_message($elements);
  _druru_prepare_user_picture($elements);

  foreach (element_children($elements) as $key) {
    $is_element = isset($elements[$key]['#type']);

    if ($is_element && $elements[$key]['#type'] == 'user_profile_category') {
      foreach ($elements[$key] as $item_key => $item_value) {

        $is_item = isset($item_value['#type']);

        if ($is_item && $item_value['#type'] == 'user_profile_item') {
          if (empty($item_value['#title'])) {
            $elements[$key][$item_key]['#title'] = $elements[$key]['#title'];
            // We add title only for first item in the group.
            break;
          }
        }
      }
    }

    $profile[$key] = $elements[$key];
  }

  $current_user = $GLOBALS['user'];
  if (in_array('Модератор', $current_user->roles) || in_array('Administrator', $current_user->roles)) {
    $profile['Профиль']['email']['#title'] = 'Email';
    $profile['Профиль']['email']['#type'] = 'user_profile_item';
    $profile['Профиль']['email']['#markup'] = $account->mail;

    $ip = _druru_get_ip_address($account->uid);

    $profile['Профиль']['ip']['#title'] = 'Last ip';
    $profile['Профиль']['ip']['#type'] = 'user_profile_item';
    $profile['Профиль']['ip']['#markup'] = $ip ?: 'ip-адрес не определен';

    $profile['Профиль']['ip_country']['#title'] = 'Страна по ip';
    $profile['Профиль']['ip_country']['#type'] = 'user_profile_item';
    $profile['Профиль']['ip_country']['#markup'] = _druru_get_country_by_ip($ip);
  }

  // Preprocess fields.
  field_attach_preprocess('user', $account, $elements, $variables);

  $variables['name'] = $account->name;
  $variables['realname'] = $account->name;

  if ($account->signature) {
    $variables['signature'] = check_markup($account->signature, $account->signature_format, '', TRUE);
  }
}

function _druru_prepare_private_message(&$elements) {
  $link = &$elements['privatemsg_send_new_message'];
  $title = '<span class="visible-xs">' . t('Send message') . '</span>';
  $link['#title'] = $title . ' ' . druru_icon('envelope');
  $link['#options']['html'] = TRUE;
  $attributes = &$link['#options']['attributes'];

  if (isset($attributes['class']) && !is_array($attributes['class'])) {
    $attributes['class'] = array($attributes['class']);
  }

  $attributes['class'][] = 'btn';
  $attributes['class'][] = 'btn-primary';
  $attributes['class'][] = 'pull-right';
}

function _druru_prepare_user_picture(&$elements) {
  $account = $elements['#account'];
  $picture = isset($account->picture) ? $account->picture : null;

  if (!empty($picture->uri)) {
    $filepath = $picture->uri;
  }
  elseif (variable_get('user_picture_default', '')) {
    $filepath = variable_get('user_picture_default', '');
  }

  if (isset($filepath)) {
    $elements['user_picture']['#markup'] = theme('image_style', array(
      'style_name' => 'avatar_profile',
      'path' => $filepath,
      'alt' => 'user-icon',
    ));
  }
}

function _druru_prepare_order(&$elements) {
  $x = -100;
  $weigts = array(
    'Персональные данные' => ++$x,
    'Контакты' => ++$x,
    'Координаты в интернете' => ++$x,
    'Предлагаю сервисы для Drupal' => ++$x,
    'Мои работы для Drupal' => ++$x,
    'Рассылки' => ++$x,
    'summary' => ++$x,
    'email' => ++$x,
    'ip' => ++$x,
    'ip_country' => ++$x,
    'Профиль' => ++$x,
    'field_promo_signature' => ++$x,
    'activity_calendar' => ++$x,
    'links' => ++$x,
  );

  foreach ($weigts as $key => $weight) {
    if(isset($elements[$key])){
      $elements[$key]['#weight'] = $weight;
    }
  }
}

function _druru_get_ip_address(string $uid) {
  return db_select('uiplog', 'n')
    ->fields('n', ['ip'])
    ->condition('n.uid', $uid)
    ->orderBy('n.timestamp', 'DESC')
    ->execute()
    ->fetchField();
}

function _druru_get_country_by_ip(string $ip) {
  if ($ip) {
    $data = json_decode(file_get_contents("http://ip-api.io/api/json/$ip"));
    return $data->country_name;
  }

  return 'Страна не определена';
}
