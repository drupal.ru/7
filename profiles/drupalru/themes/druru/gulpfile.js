var gulp          = require('gulp');
var less          = require('gulp-less');
var header        = require('gulp-header');
var rename        = require('gulp-rename');
var lessGlob      = require('gulp-less-glob');
var sourcemaps    = require('gulp-sourcemaps');
var autoprefixer  = require('gulp-autoprefixer');

// Compile LESS with autoprefixing and create sourcemaps
gulp.task('css', function() {
  return gulp
    .src('./less/style.less')
    .pipe(sourcemaps.init())
    .pipe(lessGlob())
    .pipe(less({javascriptEnabled: true})).on('error', console.log.bind(console))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./css'))
});

// Default task
gulp.task('default', gulp.series(['css']));

// Development task to launch Browsersync and watch CSS files
gulp.task('watch', gulp.series(['css'], function () {
  gulp.watch('./less/**/*.less', gulp.series(['css']));
}));