(function ($) {

  'use strict';

  Drupal.behaviors.refreshAd = {
    attach: function (context, settings) {

      // Automatically refresh banner block every 30 seconds.
      // @todo Make classes and duration of banner refresh configurable.
      $('body', context).each(function() {
        // Do not refresh banner block if theme debug enabled.
        if (!$(this).hasClass('theme-debug')) {
          const view_name = 'advertisement_blocks';
          const view_display_id = 'banner_top';
          const view_class = '.view-dntn';

          setTimeout(function(){
            $.ajax({
              url: Drupal.settings.basePath + 'views/ajax',
              type: 'post',
              data: {
                view_name: view_name,
                view_display_id: view_display_id,
              },
              dataType: 'json',
              success: function (response) {
                if (response[1] !== undefined) {
                  let viewHtml = response[1].data;
                  $(view_class).replaceWith(viewHtml);
                  Drupal.attachBehaviors();
                }
              }
            });
          }, 30000);
        }
      });
    }
  };
})(jQuery);