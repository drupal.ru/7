'use strict';

const gulp          = require('gulp');
const sass          = require('gulp-dart-sass');
const sassGlob      = require('gulp-sass-glob');
const sourcemaps    = require('gulp-sourcemaps');
const autoprefixer  = require('gulp-autoprefixer');
const include       = require('gulp-include');
const tildeImporter = require('node-sass-tilde-importer');

const jsSourcePath = [
  'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js*'
];

// Compile SCSS with autoprefixing and create sourcemaps
gulp.task('css-dev', function() {
  return gulp
    .src(['./src/sass/style.scss'])
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({importer: tildeImporter}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./assets/css'))
});

gulp.task('js-prod', function() {
  return gulp
    .src(jsSourcePath)
    .pipe(include())
    .on('error', console.log)
    .pipe(gulp.dest('./assets/js'))
});

// Compile SCSS with autoprefixing and create sourcemaps
gulp.task('css-prod', function() {
  return gulp
    .src(['./src/sass/style.scss'])
    .pipe(sassGlob())
    .pipe(sass({importer: tildeImporter}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./assets/css'))
});

// Default task
gulp.task('default', gulp.series(['css-dev', 'js-prod']));

// Development task to watch SCSS files
gulp.task('watch', gulp.series(['css-dev'], function () {
  gulp.watch(['./src/sass/**/*.scss', './src/components/**/*.scss'], gulp.series(['css-dev']));
}));

// Production task
gulp.task('prod', gulp.series(['css-prod', 'js-prod']));
