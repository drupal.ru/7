<?php require_once path_to_theme() . '/templates/molecules/layout/header.inc.php'; ?>

<?php if ($tabs['#primary'] || $tabs['#secondary']): ?>
  <div class="tabs">
    <div class="content">
      <?php print render($tabs); ?>
    </div>
  </div>
<?php endif; ?>

<?php if ($action_links): ?>
  <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>

<main>
  <section class="main-content">
    <?php print render($page['aura_content']); ?>
  </section>

  <?php print render($page['aura_sidebar']); ?>
</main>
<?php require_once path_to_theme() . '/templates/molecules/layout/footer.inc.php'; ?>
