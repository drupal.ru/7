<?php require_once path_to_theme() . '/templates/molecules/layout/header.inc.php'; ?>

<main>
  <?php require_once path_to_theme() . '/templates/pages/front/spotlight.inc.php'; ?>

  <section class="main-content">
    <?php print render($page['aura_content']); ?>
  </section>

  <?php print render($page['aura_sidebar']); ?>
</main>

<section class="why-drupal">
  <div class="content">
    <header>
      <h2>Почему Друпал</h2>
      <div>Drupal — система управления контентом (CMS) и платформа омниканального персонализированного цифрового взаимодействия (DXP), которая поможет вашей организации установить связь с пользователями и клиентами, где бы они ни находились</div>
    </header>
    <div class="drupal-powers">
      <div class="featured-powers">
        <div class="power-fact">
          <h3>Постоянное развитие</h3>
          <p>Внедрений современных решений и&nbsp;цифровых инноваций</p>
          <p>Drupal enables continuous digital innovation at leading organizations like Weather.com and NBCUniversal. With Drupal, marketers and web developers can create and manage great web, mobile and social experiences. And they can improve and adapt quickly thanks to ongoing innovation and ideas from the Drupal community.</p>
        </div>
        <div class="power-fact">
          <h3>Скорость и своевременность</h3>
          <p>Быстрое развёртывание новых проектов и&nbsp;проверка маркетинговых гипотез</p>
          <p>Speed matters in digital marketing. Digital-first organization needs a platform that helps them move quickly and capitalize on business opportunities. Drupal’s flexible platform lets marketers and developers overcome bottlenecks and delays so they can run a fast and agile team and create amazing experiences.</p>
        </div>
        <div class="power-fact">
          <h3>Масштабируемость без ограничений</h3>
          <p>От&nbsp;MVP до&nbsp;высоконагруженного сайта без&nbsp;лицензионных ограничений</p>
          <p>Drupal is enterprise-ready for the world’s busiest websites like GRAMMY.com and The Olympics where failure is not an option. And, more than just supporting high traffic sites, Drupal will scale with your business and your brands for your next-generation digital transformation and allow you to evolve down the road without skipping a beat.</p>
        </div>
        <div class="power-fact">
          <h3>Безопасность и стабильность</h3>
          <p>Выделенная команда безопасности и&nbsp;современные стандарты кода</p>
          <p>Drupal is mature, stable and designed with robust security in mind. Leading organizations around the world rely on Drupal for mission-critical sites and applications, testing its security against the most stringent standards. Many security problems are prevented entirely by Drupal’s strong coding standards and rigorous community code review process.</p>
        </div>
      </div>
      <div class="power-facts">
        <div class="power-fact">
          <h3>Маркетинг</h3>
          <p>Встроенные возможности Drupal и&nbsp;большое количество готовых решений для&nbsp;маркетинговых задач</p>
        </div>
        <div class="power-fact">
          <h3>Highload</h3>
          <p>На Drupal успешно работают высоконагруженные сайты и проекты с переодическими всплесками трафика</p>
        </div>
        <div class="power-fact">
          <h3>Стоимость владения</h3>
          <p>Разработка и поддержка проекта на Drupal значительно дешевле на горизонте нескольких лет</p>
        </div>
        <div class="power-fact">
          <h3>Гибкость</h3>
          <p>Архитектура Drupal позволяет создать как небольшой сайт, так и сложный проект</p>
        </div>
        <div class="power-fact">
          <h3>Расширяемость</h3>
          <p>Возможность использовать модули позволяет расширять базовый функционал Drupal практически безгранично</p>
        </div>
        <div class="power-fact">
          <h3>Интеграции</h3>
          <p>Email, системы учета, аналитика — все это и многое другое может быть интегрировано с Drupal</p>
        </div>
        <div class="power-fact">
          <h3>Мультиязычность</h3>
          <p>У Drupal есть переводы на 80 различных языков и более 300 модулей управления мультиязычностью!</p>
        </div>
        <div class="power-fact">
          <h3>Обеспечение подхода mobile-first</h3>
          <p>Drupal 8 создан для мира mobile-first. Все в Drupal 8 поддерживает адаптивный дизайн</p>
        </div>
        <div class="power-fact">
          <h3>Удобная работа с контентом</h3>
          <p>On-page-редактирование и возможность реализации atomic-design принципов уже реальность для Drupal8.</p>
        </div>
        <div class="power-fact">
          <h3>Документированный API</h3>
          <p>Подробная документация в руках разработчика — это быстрое и качественное решение любых задач</p>
        </div>
        <div class="power-fact">
          <h3>Стабильность</h3>
          <p>Каждый модуль на Drupal.org проходит тщательную модерацию, что обеспечивает стабильную работу вашего сайта</p>
        </div>
        <div class="power-fact">
          <h3>Скорость и производительность</h3>
          <p>Эффективные методы кеширования и сжатия делают сайт на Drupal очень быстрым</p>
        </div>
      </div>
      <div class="actions">
        <a href="/tracker/featured">Подробнее</a>
      </div>
    </div>
  </div>
  <div class="wave"></div>
  <div class="wave"></div>
  <div class="wave"></div>
</section>

<?php print views_embed_view('quotes'); ?>

<section class="drupal-targets">
  <div class="drupal-areas">
    <div class="drupal-area for-developers">
      <h3 class="title">Для разработчиков <span>Download Drupal and build the open web</span></h3>
      <div class="content">
        <ul>
          <li>Lorem reprehenderit exercitation culpa fugiat ullamco nulla incididunt culpa nulla minim anim dolore.</li>
          <li>Eu est cupidatat sunt amet ea Lorem dolore. Exercitation culpa qui nostrud mollit. Pariatur qui ad velit qui.</li>
          <li>Occaecat labore ea nostrud anim nostrud laborum reprehenderit pariatur commodo anim consectetur aliquip.</li>
          <li>Nisi officia sint tempor in dolor voluptate qui cillum sint ipsum est.</li>
          <li>Cupidatat laboris labore elit officia magna Lorem.</li>
        </ul>
        <div class="actions"><a class="btn btn-secondary">Подробнее</a></div>
      </div>
    </div>
    <div class="drupal-area for-marketers">
      <h3 class="title">Для маркетологов <span>In a world full of templates, be original</span></h3>
      <div class="content">
        <ul>
          <li>Esse ullamco nisi ex ipsum sint adipisicing minim elit.</li>
          <li>Qui nostrud sint veniam exercitation esse aliqua dolore quis sint reprehenderit officia tempor.</li>
          <li>Exercitation non et ut fugiat magna cillum consectetur eu pariatur velit elit laboris adipisicing excepteur.</li>
          <li>Aute nostrud excepteur laborum elit anim culpa reprehenderit esse nisi in fugiat nisi.</li>
          <li>Nisi enim laborum nostrud amet et eiusmod enim anim aliquip Lorem.</li>
        </ul>
        <div class="actions"><a class="btn btn-secondary">Подробнее</a></div>
      </div>
    </div>
    <div class="drupal-area for-agencies">
      <h3 class="title">Для агентств <span>Achieve your clients’ ambition</span></h3>
      <div class="content">
        <ul>
          <li>Anim eu cillum ut labore irure esse nulla qui velit consequat esse pariatur.</li>
          <li>Occaecat aute ad pariatur dolor duis elit consequat culpa nulla occaecat.</li>
          <li>Ipsum reprehenderit exercitation sint ut amet anim velit.</li>
          <li>Voluptate culpa dolor sint laborum aute voluptate amet ut nisi est fugiat.</li>
          <li>Labore est nulla excepteur ad pariatur consectetur deserunt ullamco.</li>
        </ul>
        <div class="actions"><a class="btn btn-secondary">Подробнее</a></div>
      </div>
    </div>
  </div>
</section>
<?php require_once path_to_theme() . '/templates/molecules/layout/footer.inc.php'; ?>
