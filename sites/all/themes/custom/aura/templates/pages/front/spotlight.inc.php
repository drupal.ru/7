<?php
$show_spotlight = (int) config_pages_get('spotlight', 'field_show_block');

if ($show_spotlight) {
  $title = config_pages_get('spotlight', 'field_spotlight_title');
  $text = config_pages_get('spotlight', 'field_spotlight_body');
  $background = file_create_url(config_pages_get('spotlight', 'field_spotlight_image', NULL, 0, 'uri'));
  $button_label = config_pages_get('spotlight', 'field_spotlight_cta_button');
  $nid = (int) config_pages_get('spotlight', 'field_spotlight_node', NULL, 0, 'target_id');
  $url = config_pages_get('spotlight', 'field_spotlight_cta_url', NULL, 0, 'original_url');

  $url = $url ?? (drupal_get_path_alias('node/'. $nid) ?? NULL);
  $target = $url ? ' target="_blank" ' : '';
}
?>
<?php if ($show_spotlight): ?>
  <section class="spotlight"<?php print $background ? ' style="background-image: url(' . $background . ');"' : ''; ?>>
    <div class="content">
      <h2><?php print $title; ?></h2>
      <p><?php print $text; ?></p>
      <?php if ($button_label && $url): ?>
        <div class="actions"><a <?php print $target; ?> href="<?php print $url ?>"><?php print $button_label; ?></a></div>
      <?php endif; ?>
    </div>
  </section>
<?php endif; ?>
