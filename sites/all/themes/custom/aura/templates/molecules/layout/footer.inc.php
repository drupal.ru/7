<?php
$show_sponsor_block = config_pages_get('diamond_sponsor', 'field_show_block', 0);

$diamond_sponsor = $show_sponsor_block ? [
  'text' => config_pages_render_field('diamond_sponsor', 'field_diamond_sponsor_text'),
  'logo' => config_pages_render_field('diamond_sponsor', 'field_diamond_sponsor_logo'),
  'url' => config_pages_render_field('diamond_sponsor', 'field_diamond_sponsor_url'),
] : NULL;
?>
<footer>
  <?php if ($show_sponsor_block && $diamond_sponsor): ?>
    <section class="diamond-sponsor">
      <div class="text"><?php print $diamond_sponsor['text']; ?></div>
      <div class="logo">
        <?php if ($diamond_sponsor['url']): ?>
          <a target="_blank" href="<?php print $diamond_sponsor['url']; ?>">
        <?php endif; ?>
          <?php print $diamond_sponsor['logo']; ?>
        <?php if ($diamond_sponsor['logo']): ?>
          </a>
        <?php endif; ?>
      </div>
    </section>
  <?php endif; ?>

  <div class="content">
    <div class="footer-nav">
      <div class="footer-logo">
        <a href="/">
          <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 178 38">
            <path d="M22.76.93h-4.33v9.48a10.45 10.45 0 0 0-6.92-2.46C8.33 7.95 5.7 9 3.64 11.1A10.82 10.82 0 0 0 .56 19c0 3.16 1.04 5.8 3.1 7.9a10.53 10.53 0 0 0 7.85 3.16c2.93 0 5.4-.97 7.42-2.9l.55 2.37h3.28V.93ZM11.83 25.9a6.62 6.62 0 0 1-4.91-1.96A6.78 6.78 0 0 1 4.98 19a6.72 6.72 0 0 1 6.85-6.9c1.98 0 3.62.65 4.93 1.97A6.7 6.7 0 0 1 18.74 19a6.7 6.7 0 0 1-1.98 4.93 6.65 6.65 0 0 1-4.93 1.98Z"/>
            <path d="M40.92 8.35h-1.87c-2.8 0-4.96 1-6.45 3l-.73-2.88h-2.93v21.05h4.32V18.29c0-1.92.45-3.37 1.35-4.36.9-.99 2.24-1.48 4.02-1.48h2.29v-4.1Zm19.56 19.11c-1.74 1.72-4.05 2.57-6.94 2.57-2.9 0-5.22-.85-6.97-2.57-1.74-1.7-2.6-3.99-2.6-6.83V8.47h4.31v11.55c0 1.83.47 3.27 1.41 4.32a4.92 4.92 0 0 0 3.85 1.56 4.8 4.8 0 0 0 3.82-1.58c.94-1.05 1.41-2.48 1.41-4.3V8.47h4.32v12.16c0 2.84-.87 5.12-2.6 6.83Z"/>
            <path d="M85.84 9.38a11.06 11.06 0 0 0-5.6-1.43c-1.5 0-2.9.28-4.2.82a9.99 9.99 0 0 0-3.4 2.29l-.81-2.6h-2.91v28.6h4.32V27.5c1.96 1.7 4.3 2.55 7 2.55 3.14 0 5.73-1.05 7.79-3.16 2.06-2.1 3.1-4.74 3.1-7.9 0-2.08-.47-3.96-1.4-5.65-.94-1.69-2.23-3-3.9-3.96ZM79.9 25.9a6.71 6.71 0 0 1-6.91-6.9 6.73 6.73 0 0 1 6.9-6.92A6.72 6.72 0 0 1 86.75 19a6.62 6.62 0 0 1-6.85 6.91Zm33.52-17.45 3.33.02V29.5h-3.48l-.42-2.55a10.3 10.3 0 0 1-3.4 2.28c-1.32.54-2.73.8-4.23.8-2.06 0-3.93-.47-5.6-1.42a10.4 10.4 0 0 1-3.94-3.98 11.38 11.38 0 0 1-1.43-5.66c0-2.08.48-3.95 1.43-5.63.95-1.69 2.26-3 3.94-3.96a11.13 11.13 0 0 1 5.6-1.43c1.52 0 2.94.28 4.26.82a9.99 9.99 0 0 1 3.42 2.32l.52-2.64ZM100.6 23.92a6.68 6.68 0 0 0 4.93 1.96c1.98 0 3.61-.65 4.91-1.96a6.75 6.75 0 0 0 1.96-4.95 6.68 6.68 0 0 0-6.87-6.89c-1.98 0-3.62.66-4.93 1.98a6.66 6.66 0 0 0-1.96 4.91c0 1.98.65 3.63 1.96 4.95Z"/>
            <path d="M122.94.93v28.59h4.32V.93h-4.32Zm30.4 7.42h1.86v4.1h-2.28c-1.78 0-3.12.5-4.02 1.48-.9.99-1.35 2.44-1.35 4.36v11.23h-4.32V8.47h2.93l.72 2.87c1.5-2 3.65-2.99 6.46-2.99Zm14.48 21.68c2.9 0 5.21-.85 6.95-2.57 1.74-1.7 2.6-3.99 2.6-6.83V8.47h-4.31v11.55c0 1.82-.47 3.25-1.41 4.3a4.83 4.83 0 0 1-3.83 1.58c-1.62 0-2.9-.52-3.84-1.56-.94-1.05-1.41-2.5-1.41-4.32V8.47h-4.32v12.16c0 2.84.87 5.12 2.6 6.83 1.75 1.72 4.08 2.57 6.97 2.57Zm-32.62-.51a2.72 2.72 0 1 0 0-5.44 2.72 2.72 0 0 0 0 5.44Z"/>
          </svg>
        </a>
      </div>
      <?php
        $footer_menu = menu_navigation_links('menu-footer');
        print theme('links', array('links' => $footer_menu, 'attributes' => ['class' => ['footer-menu']]));
      ?>
    </div>

    <div class="community-links">
      <a target="_blank" href="https://gitlab.com/drupal.ru/7">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-git" viewBox="0 0 16 16"><path d="M15.698 7.287 8.712.302a1.03 1.03 0 0 0-1.457 0l-1.45 1.45 1.84 1.84a1.223 1.223 0 0 1 1.55 1.56l1.773 1.774a1.224 1.224 0 0 1 1.267 2.025 1.226 1.226 0 0 1-2.002-1.334L8.58 5.963v4.353a1.226 1.226 0 1 1-1.008-.036V5.887a1.226 1.226 0 0 1-.666-1.608L5.093 2.465l-4.79 4.79a1.03 1.03 0 0 0 0 1.457l6.986 6.986a1.03 1.03 0 0 0 1.457 0l6.953-6.953a1.031 1.031 0 0 0 0-1.457"/></svg>
        Репозиторий Drupal.ru v7 (текущая)
      </a>
      <a target="_blank" href="https://gitlab.com/drupal.ru/next">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-git" viewBox="0 0 16 16"><path d="M15.698 7.287 8.712.302a1.03 1.03 0 0 0-1.457 0l-1.45 1.45 1.84 1.84a1.223 1.223 0 0 1 1.55 1.56l1.773 1.774a1.224 1.224 0 0 1 1.267 2.025 1.226 1.226 0 0 1-2.002-1.334L8.58 5.963v4.353a1.226 1.226 0 1 1-1.008-.036V5.887a1.226 1.226 0 0 1-.666-1.608L5.093 2.465l-4.79 4.79a1.03 1.03 0 0 0 0 1.457l6.986 6.986a1.03 1.03 0 0 0 1.457 0l6.953-6.953a1.031 1.031 0 0 0 0-1.457"/></svg>
        Репозиторий Drupal.ru v9 (в разработке)
      </a>
      <a target="_blank" title="Crowdin" target="_blank" href="https://l10n.drupal.ru/project/drupalru-translation-initiative">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-translate" viewBox="0 0 16 16"><path d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286H4.545zm1.634-.736L5.5 3.956h-.049l-.679 2.022H6.18z"/><path d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm7.138 9.995c.193.301.402.583.63.846-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6.066 6.066 0 0 1-.415-.492 1.988 1.988 0 0 1-.94.31z"/></svg>
        Перевод зарубежных статей
        <img src="https://d322cqt584bo4o.cloudfront.net/drupalru-translation-initiative/localized.svg">
      </a>
    </div>

    <div class="copyright">
      <p>Содержимое сайта публикуется на условиях <a href="/rules#license">CreativeCommons Attribution-ShareAlike 3.0</a> или более поздней версии.</p>
      <p>Программные коды в тексте статей — на условиях <a href="/rules#license">GNU GPL v2</a> или более поздней версии.</p>
    </div>

    <div class="drupal-trademark">Drupal – торговая марка Дриса Бёйтарта</div>
  </div>
</footer>
