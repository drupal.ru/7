<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="drupal-quotes container py-5">
  <div id="carousel-quotes" class="py-5 carousel slide xcarousel-fade carousel-dark" data-bs-ride="false" data-bs-pause="hover">
    <div class="carousel-inner">
      <?php foreach ($rows as $id => $row): ?>
        <div class="carousel-item<?php $id === 0 ? print ' active' : ''; ?>">
          <?php print $row; ?>
        </div>
      <?php endforeach; ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carousel-quotes" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carousel-quotes" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
</section>