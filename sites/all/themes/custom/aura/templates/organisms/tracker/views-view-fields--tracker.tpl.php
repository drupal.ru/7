<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 * @todo Create variables for needed values from $fields to make code cleaner.
 */

// Create variables from the keys and values of the elements of $fields array.
extract(array_combine(array_keys($fields), array_column($fields, 'content')));
?>
<div class="meta">
  <a class="author" <?php if ($field_promo_signature) print 'title="' . $field_promo_signature . '"'; ?> href="<?php print url('user/'. $uid); ?>"><?php print $picture; ?> <span class="username"><?php print $name; ?></span></a>
  <span class="reactions">
    <?php foreach ($reactions as $reaction): ?>
      <?php print $reaction; ?>
    <?php endforeach; ?>
  </span>
  <span class="count-comments count-<?php print $comment_count; ?><?php print ((int) $new_comments > 0) ? ' has-new-comments' : ''; ?>">
    <?php if ($comment_count == 0 || ($comment_count != $new_comments)): ?>
      <span class="count-comments-total"><?php print $comment_count; ?></span>
    <?php endif; ?>
    <?php if ($new_comments > 0): ?>
      <span class="count-comments-new">+<?php print $new_comments; ?></span>
    <?php endif; ?>
  </span>
  <?php if ($has_accepted_answer): ?>
    <span class="has-accepted-answer"><span class="label">Решено</span></span>
  <?php endif; ?>
  <?php if ($new): ?>
    <span class="status is-new"></span>
  <?php endif; ?>
</div>
<div class="content">
  <?php if ($display_summary): ?>
    <h3 class="title"><a href="<?php print $path; ?>"><?php print $title; ?></a></h3>
    <div class="text"><?php print $body; ?></div>
  <?php else: ?>
    <h3 class="title"><a href="<?php print $path; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>
</div>
<div class="meta">
  <div class="date"><?php print $created; ?></div>
</div>
