<?php

/**
 * @file
 * Theme functions
 */

function aura_theme() {
  return [
    'button_button' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Alter CSS files before they are output on the page.
 */
function aura_css_alter(&$css) {
  $exclude = array(
    'misc/vertical-tabs.css' => FALSE,
    'modules/aggregator/aggregator.css' => FALSE,
    'modules/block/block.css' => FALSE,
    'modules/book/book.css' => FALSE,
    'modules/comment/comment.css' => FALSE,
    'modules/dblog/dblog.css' => FALSE,
    'modules/file/file.css' => FALSE,
    'modules/filter/filter.css' => FALSE,
    'modules/forum/forum.css' => FALSE,
    'modules/help/help.css' => FALSE,
    'modules/menu/menu.css' => FALSE,
    'modules/node/node.css' => FALSE,
    'modules/openid/openid.css' => FALSE,
    'modules/poll/poll.css' => FALSE,
    'modules/profile/profile.css' => FALSE,
    'modules/search/search.css' => FALSE,
    'modules/statistics/statistics.css' => FALSE,
    'modules/syslog/syslog.css' => FALSE,
    'modules/system/admin.css' => FALSE,
    'modules/system/maintenance.css' => FALSE,
    'modules/system/system.css' => FALSE,
    'modules/system/system.admin.css' => FALSE,
    'modules/system/system.base.css' => FALSE,
    'modules/system/system.maintenance.css' => FALSE,
    'modules/system/system.menus.css' => FALSE,
    'modules/system/system.messages.css' => FALSE,
    'modules/system/system.theme.css' => FALSE,
    'modules/taxonomy/taxonomy.css' => FALSE,
    'modules/tracker/tracker.css' => FALSE,
    'modules/update/update.css' => FALSE,
    'modules/user/user.css' => FALSE,
    'sites/all/modules/contrib/bueditor/bueditor.css' => FALSE,
    'sites/all/modules/contrib/comment_notify/comment_notify.css' => FALSE,
    'sites/all/modules/contrib/flag/theme/flag.css' => FALSE,
    'sites/all/modules/contrib/geshifilter/geshifilter.css' => FALSE,
    'sites/all/modules/custom/resolve/resolve.css' => FALSE,
  );
  $css = array_diff_key($css, $exclude);
}

function aura_page_alter(&$page) {
  // Replace default message and it's title.
  if (drupal_is_front_page()) {
    $page['content']['system_main'] = '';
  }
}

function aura_preprocess_views_view(&$variables) {
  $view = $variables['view'];

  if (in_array($view->name, ['tracker', 'tracker_new', 'tracker_my', 'tracker_featured'])) {
    $variables['theme_hook_suggestions'][] = 'views_view__tracker';
  }
}

function aura_preprocess_views_view_unformatted(&$variables) {
  $view = $variables['view'];
  // $rows = $variables['rows'];
 // $variables['classes_array'] = [];
  $variables['classes'] = [];
  $results = $variables['view']->result;

  // if (in_array($view->name, ['events_upcoming', 'events_upcoming_block', 'events_past'])) {
  //   foreach ($result as $key => $result) {
  //     $variables['classes_array'][$key] = 'event';
  //   }
  // }

  // if ($view->name == 'documentation_categories') {
  //   $variables['raw_result'] = $variables['view']->style_plugin->rendered_fields;
  // }

  if (in_array($view->name, ['tracker', 'tracker_new', 'tracker_my', 'tracker_featured'])) {
    foreach ($results as $key => $result) {
      $variables['classes'][$key][] = 'node';
      $variables['classes'][$key][] = 'mini';
      $variables['classes'][$key][] = $result->node_type;

      if (dru_antispam_has_verified($result->users_node_uid) === FALSE ) {
        $variables['classes'][$key][] = 'is-unverified';
      }

      $variables['classes_array'][$key] = isset($variables['classes'][$key]) ? implode(' ', $variables['classes'][$key]) : '';
    }

    $variables['theme_hook_suggestions'][] = 'views_view_unformatted__tracker';
  }
}

function aura_preprocess_views_view_fields(&$variables) {
  $view = $variables['view'];
  $row = $variables['row'];

  if (in_array($view->name, ['tracker', 'tracker_new', 'tracker_my', 'tracker_featured'])) {
    $nid = $row->nid;
    $variables['display_summary'] = _aura_display_summary($nid);
    $variables['has_accepted_answer'] = _aura_node_has_accepted_answer($nid);

    if ($view->current_display == 'block_pinned') {
      $variables['display_summary'] = TRUE;
    }

    $variables['reactions']['thanks'] = _aura_static_reaction('thx_nodes', $nid);

    // New/updated mark
    $variables['new'] = FALSE;
    $node_mark = node_mark($row->nid, $row->node_changed);
    if (in_array($node_mark, [MARK_NEW, MARK_UPDATED])) {
      $variables['new'] = TRUE;
    }

    $variables['theme_hook_suggestions'][] = 'views_view_fields__tracker';
  }
}

function aura_preprocess_region(&$variables) {
  if ($variables['region'] == 'aura_sidebar') {
    $variables['theme_hook_suggestions'][] = 'region__' . 'sidebar';
  }
}

function _aura_display_summary($nid) {
  $display_summary_nids = config_pages_get('display_summary', 'field_display_summary_nodes', '', []);
  $display_summary_nids = array_column($display_summary_nids, 'target_id');

  return in_array($nid, $display_summary_nids);
}

/**
 * Button theme function.
 */
function aura_button_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  if (empty($element['#text'])) {
    $element['#text'] = $element['#value'];
  }

  return '<button' . drupal_attributes($element['#attributes']) . '>' . $element['#text'] . '</button>';
}

/**
 * Returns HTML for status and/or error messages, grouped by type.
 */
function aura_status_messages($variables) {
  $display = $variables['display'];
  $output = '';
  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"system-message {$type}\"><div class=\"content\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= reset($messages);
    }
    $output .= "</div></div>\n";
  }

  return $output;
}

/**
 * Returns HTML for primary and secondary local tasks.
 */
function aura_menu_local_tasks(&$variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs-primary">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs-secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Returns HTML for a single local task link.
 */
function aura_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];
  $link['localized_options']['html'] = TRUE;

  if (!empty($variables['element']['#active'])) {

    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link_text = t('!local-task-title!active', [
      '!local-task-title' => $link['title'],
      '!active' => $active,
    ]);
  }
  return '<li' . (!empty($variables['element']['#active']) ? ' class="active"' : '') . '>' . l('<span>' . $link_text . '</span>', $link['href'], $link['localized_options']) . "</li>\n";
}

/**
* Implements hook_menu_local_tasks_alter().
*/
function aura_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if (strpos($root_path, 'user/%') !== FALSE) {
    foreach ($data['tabs'] as $set_key => $tabs) {
      foreach ($tabs['output'] as $tab_key => $tab) {
        switch ($tab['#link']['path']) {
          case 'user/%/view':
            $data['tabs'][$set_key]['output'][$tab_key]['#link']['title'] = 'Профиль';
            break;
          case 'user/%/edit':
            $data['tabs'][$set_key]['output'][$tab_key]['#link']['title'] = 'Настройки';
            $data['tabs'][$set_key]['output'][$tab_key]['#weight'] = '100';
            break;
          case 'user/%/spambot':
            $data['tabs'][$set_key]['output'][$tab_key]['#link']['title'] = 'Спамбот';
            $data['tabs'][$set_key]['output'][$tab_key]['#weight'] = '101';
            break;
          case 'user/%/devel':
            $data['tabs'][$set_key]['output'][$tab_key]['#weight'] = '102';
            break;
        }
      }
    }
  }
}

/**
 * Returns HTML for a query pager.
 */
function aura_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = 5;
  global $pager_page_array, $pager_total;

  // Nothing to do if there is no pager.
  if (!isset($pager_page_array[$element]) || !isset($pager_total[$element])) {
    return;
  }

  // Nothing to do if there is only one page.
  if ($pager_total[$element] <= 1) {
    return;
  }

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next ›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('page-item', 'page-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('page-item', 'page-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('page-item', 'disabled', 'page-ellipsis'),
          'data' => '<span class="page-link"><span>…</span></span>',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('page-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('page-item', 'active', 'page-current'),
            'data' => '<span class="page-link"><span>' . $i . '</span></span>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('page-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('page-item', 'disabled', 'page-ellipsis'),
          'data' => '<span class="page-link"><span>…</span></span>',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('page-item', 'page-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('page-item', 'page-last'),
        'data' => $li_last,
      );
    }
    return '<nav aria-label="' . t('Pages') . '">' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pagination')),
    )) . '</nav>';
  }
}

/**
 * Returns HTML for a list or nested list of items.
 */
function aura_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }

  return $output;
}

/**
 * Returns HTML for a link to a specific query result page.
 */
function aura_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  $attributes['href'] = url($_GET['q'], array('query' => $query));
  $attributes['class'][] = 'page-link';
  return '<a' . drupal_attributes($attributes) . '><span>' . check_plain($text) . '</span></a>';
}

/**
 * Implements hook_preprocess_node().
 */
function aura_preprocess_node(&$variables) {
  global $user;
  $content = &$variables['content'];
  $comments = isset($variables['content']['comments']['comments'])? $variables['content']['comments']['comments'] : [];
  $view_mode = $variables['view_mode'];
  $node = $variables['node'];

  $variables['date'] = _aura_format_date_aging($variables['created']);

  $variables['attributes_array']['data-node-id'] = $node->nid;

  if (!$variables['status']) {
    $variables['classes_array'][] = 'is-unpublished';
  }

  $variables['new_comments'] = comment_num_new($node->nid);

  $variables['accepted_answer_link'] = '';
  $variables['has_accepted_answer'] = _aura_node_has_accepted_answer($node->nid);
  if ($variables['has_accepted_answer']) {
    $resolved_comment_cid = $variables['content']['resolved_comment']['#comment']->cid;
    $resolved_comment_index = array_search($resolved_comment_cid, array_keys($comments));
    // Add link to the 'accepted' comment only if node has more than 5 comments,
    // and the 'accepted' comment is the 4th in the list.
    if ($node->comment_count > 5 && $resolved_comment_index > 3) {
      $accepted_answer_uri = comment_uri($content['resolved_comment']['#comment']);
      $variables['accepted_answer_link'] = t('Go to <a href="@url">accepted answer</a>', ['@url' => '#' . $accepted_answer_uri['options']['fragment']]);
    }
    // @note Workaround to remove 'resolved' commend added by module resolve.
    unset($content['resolved_comment']);
  }

  $variables['classes_array'][] = $variables['view_mode'];

  if (in_array($variables['type'], ['blog', 'guide'])) {
    $variables['content']['links']['blog']['#links']['blog_usernames_blog']['title'] = t('Blog');
    $variables['content']['links']['blog']['#links']['blog_usernames_blog']['href'] = '/user/' . $variables['uid'] . '/blog';
  }

  // // Render blocks assigned to 'content_third' region
  // // (between node and comments) for node full view
  // @todo Replace "Between ad" with normal node (or custom entity)
  // if ($variables['page'] && $content_third = block_get_blocks_by_region('content_third')) {
  //   $variables['content_third'] = $content_third;
  // }

  // if (!dru_antispam_has_verified($variables['uid'])) {
  //   $variables['classes_array'][] = 'is-unverified';
  // }

  $variables['user_picture'] = theme('user_picture', ['account' => $node, 'style' => $view_mode == 'full' ? 'avatar' : 'avatar_menu']);

  // Reactions
  unset($content['links']['flag']['#links']['flag-thx_nodes']);
  if ($user->uid == $node->uid || $user->uid == 0 || $view_mode != 'full') {
    $variables['reactions']['thanks'] = _aura_static_reaction('thx_nodes', $node->nid);
  }
  else {
    $variables['reactions']['thanks'] = flag_create_link('thx_nodes', $node->nid);
  }

  // New/updated mark
  $variables['new'] = FALSE;
  $node_mark = node_mark($node->nid, $node->changed);
  if (in_array($node_mark, [MARK_NEW, MARK_UPDATED])) {
    $variables['new'] = TRUE;
  }

  foreach ($variables['theme_hook_suggestions'] as $suggestion) {
    $variables['theme_hook_suggestions'][] = $suggestion . '__' . $view_mode;
  }
  $variables['theme_hook_suggestions'][] = 'node__' . $view_mode;
}

/**
 * Format timestamp based on its oldness.
 */
function _aura_format_date_aging($timestamp) {
  if (date('Ymd') == date('Ymd', $timestamp)) {
    return format_date($timestamp, 'current_day');
  }
  elseif (date('Y') == date('Y', $timestamp)) {
    return format_date($timestamp, 'current_year');
  }
  else {
    return format_date($timestamp, 'years');
  }
}

/**
 * Implements hook_preprocess_field().
 * @todo Refactor to use css classes
 */
function aura_preprocess_field(&$variables) {
  $element = $variables['element'];
  $items   = $variables['items'];
  // $label   = &$variables['label'];

  // // Assign class name to body of nodes and comments
  // if (in_array($element['#field_name'], ['body', 'comment_body'])) {
  //   $variables['classes_array'] = [];
  //   $variables['classes_array'][] = $element['#entity_type'] . '__body';
  // }

  if (in_array($element['#field_name'], ['taxonomy_vocabulary_7', 'taxonomy_vocabulary_8', 'taxonomy_forums'])) {
    module_load_include('inc', 'pathauto');
    foreach ($items as $key => $item) {
      $vocabulary = taxonomy_vocabulary_load($item['#options']['entity']->vid);
      $variables['items'][$key]['#attributes']['class'][] = pathauto_cleanstring($vocabulary->name) . '--' .  pathauto_cleanstring($item['#title']);
    }
    // Set single tpl for group of taxonomy fields
    $variables['theme_hook_suggestions'][] = 'field__group_taxonomy';
    // $variables['theme_hook_suggestions'][] = 'field__group_taxonomy__' . $element['#entity_type'];
    // $variables['theme_hook_suggestions'][] = 'field__group_taxonomy__' . $element['#entity_type'] . '__' . $element['#view_mode'];
  }

  // if ($element['#field_name'] == 'field_guide_authors') {
  //   $authors = $variables['items'];

  //   // Prepare variable for 'promo signature' to be used in field template
  //   foreach ($authors as $key => $author) {
  //     $promo_signature = !empty($author['#item']['entity']->field_promo_signature) ? $author['#item']['entity']->field_promo_signature[LANGUAGE_NONE][0]['safe_value'] : NULL;
  //     $variables['items'][$key]['promo_signature'] = $promo_signature ? ' <span class="user__promo">' . $promo_signature . '</span>' : '';
  //   }

  //   // Change field label according to number of referenced users
  //   $variables['label'] = count($variables['items']) > 1 ? t('Authors') : t('Author');
  // }
}

/**
 * Process variables for user-picture.tpl.php.
 */
function aura_preprocess_user_picture(&$variables) {
  $variables['user_picture'] = '';
  $style = $variables['style'] ?? variable_get('user_picture_style', '');

  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if (!empty($account->picture)) {
      // @TODO: Ideally this function would only be passed file objects, but
      // since there's a lot of legacy code that JOINs the {users} table to
      // {node} or {comments} and passes the results into this function if we
      // a numeric value in the picture field we'll assume it's a file id
      // and load it for them. Once we've got user_load_multiple() and
      // comment_load_multiple() functions the user module will be able to load
      // the picture files in mass during the object's load process.
      if (is_numeric($account->picture)) {
        $account->picture = file_load($account->picture);
      }
      if (!empty($account->picture->uri)) {
        $filepath = $account->picture->uri;
      }
    }
    elseif (variable_get('user_picture_default', '')) {
      $filepath = variable_get('user_picture_default', '');
    }
    if (isset($filepath)) {
      $alt = t("@user's picture", array('@user' => format_username($account)));
      // If the image does not have a valid Drupal scheme (for eg. HTTP),
      // don't load image styles.
      if (module_exists('image') && file_valid_uri($filepath) && $style) {
        $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      else {
        $variables['user_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
    }
  }
}

/**
 * Check if node has accepted answer.
 */
function _aura_node_has_accepted_answer($nid) {
  $query = "SELECT nid FROM {resolved} WHERE nid = :nid";
  $resolved = db_query($query, [':nid' => $nid])->fetchCol();
  return count($resolved) ? TRUE: FALSE;
}

function aura_preprocess_html(&$variables) {
  $variables['classes_array'][] = variable_get('theme_debug', NULL) ? 'theme-debug' : NULL;
}

function aura_preprocess_comment(&$variables) {
  global $user;
  $comment = $variables['elements']['#comment'];
  $node = $variables['elements']['#node'];
  $content = &$variables['content'];
  $view_mode = $variables['elements']['#view_mode'];

  $variables['date'] = _aura_format_date_aging($comment->changed);

  $variables['attributes_array']['data-comment-id'] = $variables['comment']->cid;

  // // Remove author avatar and name in comment teasers
  // $variables['show_author'] = ($view_mode == 'teaser') ? FALSE : TRUE;

  $uri = entity_uri('comment', $comment);
  $uri['options'] += [
    'attributes' => [
      'class' => [
        'permalink',
      ],
      'rel' => 'bookmark',
    ],
  ];

  $variables['title'] = l($node->title, $uri['path'], $uri['options']);

  $variables['permalink'] = l("", $uri['path'], $uri['options']);

  if ($comment->status == COMMENT_NOT_PUBLISHED) {
    $variables['classes_array'][] = 'is-unpublished';
  }

  if ($variables['new']) {
    $variables['classes_array'][] = 'is-new';
  }

  if (!$comment->uid) {
    $variables['classes_array'][] = 'by-anonymous';
  }
  else {
    if ($comment->uid == $variables['node']->uid) {
      $variables['classes_array'][] = 'by-node-author';
    }
    if ($comment->uid == $variables['user']->uid) {
      $variables['classes_array'][] = 'by-viewer';
    }
  }

  $variables['is_accepted_answer'] = FALSE;
  if (isset($comment->best) && $comment->best) {
    $variables['classes_array'][] = 'is-accepted-answer';
    $variables['is_accepted_answer'] = TRUE;
  }

  $variables['classes_array'][] = $view_mode;

  //_druru_wrap_claim($content, 'comment', $variables['id']);

  if (!dru_antispam_has_verified($comment->uid)) {
    $variables['classes_array'][] = 'is-unverified';
  }

  // Reactions
  unset($content['links']['flag']['#links']['flag-thx_comments']);
  if ($user->uid == $comment->uid || $user->uid == 0) {
    $variables['reactions']['thanks'] = _aura_static_reaction('thx_comments', $comment->cid);
  }
  else {
    $variables['reactions']['thanks'] = flag_create_link('thx_comments', $comment->cid);
  }

  $variables['user_picture'] = theme('user_picture', ['account' => $comment, 'style' => 'avatar_menu']);
}

function aura_form_comment_form_alter(&$form, &$form_state, $form_id) {
  if(isset($form['author'])){
    $form['author']['#access'] = false;
  }
}

function aura_preprocess_flag(&$variables) {
  $flag = $variables['flag'];
  $entity_id = $variables['entity_id'];
  $action = $variables['action'];

  if (in_array($flag->name, ['thx_nodes', 'thx_comments'])) {
    $flag_count = _drurum_get_flag_count($flag->fid, $entity_id);

    $variables['flag_wrapper_classes_array'][] = 'reaction';
    $variables['flag_wrapper_classes_array'][] = 'thanks';
    $variables['flag_wrapper_classes_array'][] = 'count-' . $flag_count;
    $variables['flag_wrapper_classes_array'][] = ($action == 'flag' ? 'unflagged' : 'flagged');

    $variables['link_title'] = $variables['link_text'];
    $variables['link_text'] = $flag_count;

    $variables['theme_hook_suggestions'][] = 'flag__reaction';
  }
}

/**
 * Return static HTML for specified reaction/flag.
 */
function _aura_static_reaction($flag_name, $entity_id) {
  global $user;
  $flag = flag_load($flag_name);
  $entity_type = $flag->entity_type;
  $flag_count = _drurum_get_flag_count($flag->fid, $entity_id);
  $has_current_user_reaction = FALSE;

  if (user_is_logged_in()) {
    // Mark entities on which user reacted previously.
    $reacted_users = flag_get_entity_flags($entity_type, $entity_id, $flag_name);
    $has_current_user_reaction = array_key_exists($user->uid, $reacted_users);
  }

  // @todo Refactor to use 'children' in $reaction_status as soon as issue
  // 2981726 get fixed (https://www.drupal.org/project/drupal/issues/2981726).
  $flag = [
    '#type' => 'html_tag',
    '#tag' => 'span',
    '#value' => $flag_count,
    '#attributes' => [
      'class' => [
        'flag',
      ],
    ],
  ];
  $flag = render($flag);

  $reaction_status = [
    '#type' => 'html_tag',
    '#tag' => 'span',
    '#value' => $flag,
    '#attributes' => [
      'class' => [
        'reaction',
        'thanks',
        'count-' . $flag_count,
        $has_current_user_reaction ? 'flagged' : NULL,
      ],
    ]
  ];
  $reaction_status = render($reaction_status);

  return $reaction_status;
}
