CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

A module with garland and snow decoration that creates a long lasting atmosphere
of New Years for you and the users of your website.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/happy_new_year

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/happy_new_year


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

    1. Navigate to Modules and enable the module.
    2. Navigate to Configuration > Media > Happy New Year! to
       configure the module.
    3. To enable a working period, check the select box Enable Working Period. A
       conditional field will open and there will be dropdowns to select start
       and end dates.
       If left blank, the module will be enabled at all times.
    4. Select if you want garland or snow. Choose your colors with the color
       wheel or be entering the hexadecimal code.
    5. There is an option to use minified libraries is possible.
    6. Save configuration.


MAINTAINERS
-----------

Current maintainers:
 * Andrei Ivnitskii - https://www.drupal.org/u/ivnish
