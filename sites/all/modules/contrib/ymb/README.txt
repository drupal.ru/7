CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module allows creation of blocks with form for raising money to your Yandex
Money Wallet.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/ymb

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/ymb


REQUIREMENTS
------------

block (drupal core)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Modules and enable the module.
    2. Navigate to Administration > Configuration > Content > Yandex Money 
       Blocks and set count of blocks.
    3. Navigate to Administration > Structure > Blocks and add YMB-blocks
       to your regions.


MAINTAINERS
-----------

Current maintainers:
 * Andrei Ivnitskii - https://www.drupal.org/u/ivnish
