<?php

/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * CUstom variables:
 *
 * - $description
 * - $sum
 * - $receiver
 * - $target
 * - $url
 * - $delta
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if ($block->subject): ?>
    <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
  <?php endif;?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <div class='ymb-form'>
      <p><?php print render($description); ?></p>
      <form method='POST' action='https://yoomoney.ru/quickpay/confirm.xml'>
        <div class='form-item form-type-textfield form-item-sum'>
          <label for='edit-sum--<?php print render($delta); ?>'><?php print t('Amount'); ?> <span class='form-required' title='<?php print t('This field is required.'); ?>'>*</span></label>
          <input id='edit-sum--<?php print render($delta); ?>' name='sum' value='<?php print render($sum); ?>' data-type='number' class='form-text required'>
        </div>
        <div class='form-item form-type-radios form-item-payment-type'>
          <div id='edit-payment-type--<?php print render($delta); ?>' class='form-radios'>
            <div class='form-item form-type-radio form-item-payment-type'>
              <input id='edit-payment-type-pc--<?php print render($delta); ?>' type='radio' name='paymentType' value='PC' class='form-radio' checked><label class='option' for='edit-payment-type-pc--<?php print render($delta); ?>'><?php print t('Yandex.Money'); ?></label>
            </div>
            <div class='form-item form-type-radio form-item-payment-type'>
              <input id='edit-payment-type-ac--<?php print render($delta); ?>' type='radio' name='paymentType' value='AC' class='form-radio'><label class='option' for='edit-payment-type-ac--<?php print render($delta); ?>'><?php print t('Bank card'); ?></label>
            </div>
          </div>
        </div>
        <div class='form-actions form-wrapper' id='edit-actions--<?php print render($delta); ?>'>
          <input id='edit-submit--<?php print render($delta); ?>' type='submit' value='<?php print t('Pay'); ?>' class='form-submit'>
        </div>
        <input type='hidden' name='receiver' value='<?php print render($receiver); ?>'>
        <input type='hidden' name='targets' value='<?php print render($target); ?>'>
        <input type='hidden' name='quickpay-form' value='donate'>
        <input type='hidden' name='successURL' value='<?php print render($url); ?>'>
      </form>
    </div>
  </div>
</div>
