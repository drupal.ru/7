<?php

/**
 * @file
 * Contains the boolean flagged field handler.
 */

/**
 * Views field handler for the flagged field.
 *
 * @ingroup views
 */
class flag_handler_field_ip_address extends views_handler_field {

  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
    $this->additional_fields['ip_address'] = array(
      'table' => 'flagging',
      'field' => 'ip_address',
    );
  }

  /**
   * Loads additional fields.
   */
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $value = $this->get_value($values, 'ip_address');
    return $value;
  }

}
