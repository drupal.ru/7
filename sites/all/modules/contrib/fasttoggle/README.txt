CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Fasttoggle is a Drupal module that simplifies and speeds the task of site
administration providing ajax toggling of both administrative settings and
content values.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/fasttoggle

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/fasttoggle


REQUIREMENTS
------------

jquery_update - https://www.drupal.org/project/jquery_update


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.


CONFIGURATION
-------------

 * Navigate to Modules and enable the module.
 * Navigate to Configuration > System > Fasttoggle
 * Navigate to Structure > Content Types and turning on 
   Fasttoggle in the your content type


MAINTAINERS
-----------

Current maintainers:
 * Andrei Ivnitskii - https://www.drupal.org/u/ivnish
