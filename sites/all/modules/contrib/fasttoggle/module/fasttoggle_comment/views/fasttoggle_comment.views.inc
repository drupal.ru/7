<?php

/**
 * @file
 * Views data definitions for Fasttoggle.
 *
 * This file defines how Views should get Fasttoggle data.
 */

/**
 * Implements hook_views_data_alter().
 */
function fasttoggle_comment_views_data_alter(&$data) {

  $data['comment']['fasttoggle_status'] = array(
    'field' => array(
      'title' => t('Approve link (Fasttoggle)'),
      'help' => t('Provide a simple link to toggle approval of a comment.'),
      'handler' => 'fasttoggle_comment_views_handler_field_comment_link',
    ),
  );
}
