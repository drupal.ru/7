CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Send notifications to telegram via Rules.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/rules_telegram

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/rules_telegram


REQUIREMENTS
------------

rules - https://drupal.org/project/rules


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

    1. Navigate to Modules and enable the module.
    2. Navigate to Configuration > Workflow > Rules Telegram and set proxy 
       server for telegram (if needed)
    3. Navigate to Configuration > Workflow > Rules and create your rules


MAINTAINERS
-----------

Current maintainers:
 * Andrei Ivnitskii - https://www.drupal.org/u/ivnish
