<?php

/**
 * Called after the display's pre_execute phase but before the build process.
 *
 * @param object $view
 *   The view object about to be processed.
 */
function aura_aux_views_pre_build(&$view) {
  global $theme;

  if ($theme == 'aura') {
    if ($view->name == 'tracker_featured' && $view->current_display == 'block_featured') {
      $pinned_nids = config_pages_get('pinned', 'field_pinned_nodes', NULL, [], 'target_id');
      $spotlight_nid = config_pages_get('spotlight', 'field_spotlight_node', NULL, 0, 'target_id');

      $exclude_nids = [...$pinned_nids, $spotlight_nid];
      if ($exclude_nids) {
        $view->args[0] = implode("+", $exclude_nids);
      }
    }
  }
}
