<?php

namespace Drupal\drurum\DOMDocument;

use DOMDocument;
use DOMNode;

/**
 * Class DruDOMDocument.
 *
 * @package Drupal\drurum\DOMDocument
 */
class DruDOMDocument extends DOMDocument {

  /**
   *
   */
  public function saveHTML(DOMNode $node = NULL) {
    return preg_replace('/^<!DOCTYPE.+?>/',
      '',
      str_replace(['<html>', '</html>', '<body>', '</body>'], ['', '', '', ''], parent::saveHTML()));
  }

}
